# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

import logging
logger = logging.getLogger(__name__)

from openerp import models, fields, api, _

class res_partner(models.Model):
    _inherit = 'res.partner'

    company_registry = fields.Char('Company Registry', size=64, )
    einvoice_address = fields.Char('eInvoice', size=20, help='For einvoice address')
    einvoice_operator = fields.Char('eInvoice operator', size=20, help='For einvoice operator address')

# link partner fields to company as well
class res_company(models.Model):
    _inherit = 'res.company'
        
    company_registry = fields.Char("Company Registry", related='partner_id.company_registry')
    einvoice_address = fields.Char('eInvoice', related='partner_id.einvoice_address',
        help='For einvoice address')
    einvoice_operator = fields.Char('eInvoice operator', related='partner_id.einvoice_operator',
        help='For einvoice operator address')
    
